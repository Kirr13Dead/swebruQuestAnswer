<?php
include_once "report.php";
include_once "dataSource.php";

class PaymentReport extends report
{

    const CSV = 'csv';

    public function initData()
    {
        $dataBase = new  dataSource();
        $dataBase->loadFixtures();

    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    public function createReport()
    {
        $dataBase = new  dataSource();
        $this->setData($dataBase->getReportData());

    }

    public function printResult(string $template)
    {
        parent::printResult($template);
    }


}