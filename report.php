<?php

abstract class report
{
    //данные для отчета
    public $data = [];

    //инициализация данных с помощью DataSource
    abstract public function initData();

    //выборка и подготовка данных
    abstract public function createReport();

    //вывод данных
    public function printResult(string $template)
    {
        switch ($template) {
            case 'csv' :
                echo $this->getCSV();
                break;
            default:
                break;
        }
    }

    /**
     * @return string
     */
    public function getCSV()
    {
        header('Content-Description: File Transfer');
        header("Cache-Control: public");
        header('Content-Disposition: attachement;filename="test.csv";');
        header('Content-Type: application/csv; charset=UTF-8');

        ob_start();
        $df = fopen("php://output", 'w');
        foreach ($this->data as $key => $item) {
            fputcsv($df, [$item['pdate'], $item['pamount'], $item['psum'],], "\t");
        }
        fclose($df);
        return ob_get_clean();
    }
}


