<?php
require_once "PaymentReport.php";

$report = new PaymentReport();
$report->initData();
$report->createReport();
$report->printResult('csv');
