<?php

class dataSource
{
    private $databaseConnection;

    public function loadFixtures()
    {
        $this->createDatabaseConnection();
        $db = $this->getDatabaseConnection();
        $sql = file_get_contents('database.sql');
        $db->exec($sql);
    }

    protected function getDatabaseConnection()
    {
        return $this->databaseConnection;
    }

    private function createDatabaseConnection()
    {
        $this->databaseConnection = new Pdo('sqlite:memory');
        $this->databaseConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @return mixed
     */
    public function getReportData()
    {
        $this->createDatabaseConnection();
        $db = $this->getDatabaseConnection();
        $sql = 'SELECT strftime(\'%m.%Y\', p.create_ts) pdate,
                       COUNT(p.amount) AS pamount , 
                       SUM(p.amount) psum 
                  FROM payments as p
                   LEFT JOIN documents d ON d.payment_id = p.id
                  WHERE d.id IS NULL
                 GROUP BY strftime(\'%m.%Y\', p.create_ts)';

        $result = $db->query($sql);

        return $result->fetchAll();
    }
}
